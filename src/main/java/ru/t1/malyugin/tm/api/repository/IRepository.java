package ru.t1.malyugin.tm.api.repository;

import ru.t1.malyugin.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    void clear();

    int getSize();

    M add(M model);

    M remove(M model);

    M removeById(String id);

    M removeByIndex(Integer index);

    void removeAll(List<M> models);

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    M findOneById(String id);

    M findOneByIndex(Integer index);

}