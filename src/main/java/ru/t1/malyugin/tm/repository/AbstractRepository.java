package ru.t1.malyugin.tm.repository;

import org.apache.commons.lang3.StringUtils;
import ru.t1.malyugin.tm.api.repository.IRepository;
import ru.t1.malyugin.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected final List<M> models = new ArrayList<>();

    @Override
    public void clear() {
        models.clear();
    }

    @Override
    public int getSize() {
        return models.size();
    }

    @Override
    public M add(final M model) {
        models.add(model);
        return model;
    }

    @Override
    public M remove(final M model) {
        models.remove(model);
        return model;
    }

    @Override
    public M removeById(final String id) {
        final M model = findOneById(id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public M removeByIndex(final Integer index) {
        final M model = findOneByIndex(index);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public List<M> findAll() {
        return models;
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        return models.stream().sorted(comparator).collect(Collectors.toList());
    }

    @Override
    public M findOneById(final String id) {
        return models.stream().filter(model -> StringUtils.equals(id, model.getId())).findAny().orElse(null);
    }

    @Override
    public M findOneByIndex(final Integer index) {
        return models.get(index);
    }

    @Override
    public void removeAll(final List<M> models) {
        this.models.removeAll(models);
    }

}