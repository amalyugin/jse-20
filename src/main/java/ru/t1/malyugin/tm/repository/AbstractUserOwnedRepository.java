package ru.t1.malyugin.tm.repository;

import org.apache.commons.lang3.StringUtils;
import ru.t1.malyugin.tm.api.repository.IUserOwnerRepository;
import ru.t1.malyugin.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M>
        implements IUserOwnerRepository<M> {

    @Override
    public void clear(final String userId) {
        final List<M> modelsToRemove = findAll(userId);
        removeAll(modelsToRemove);
    }

    @Override
    public int getSize(final String userId) {
        return findAll(userId).size();
    }

    @Override
    public M add(final String userId, final M model) {
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public M remove(final String userId, final M model) {
        return removeById(userId, model.getId());
    }

    @Override
    public M removeById(final String userId, final String id) {
        final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) {
        final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public List<M> findAll(final String userId) {
        return models.stream()
                .filter(m -> StringUtils.equals(userId, m.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        return findAll(userId).stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public M findOneById(final String userId, final String id) {
        return findAll(userId).stream()
                .filter(m -> StringUtils.equals(id, m.getId()))
                .findAny().orElse(null);
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

}